import 'package:allergify/models/intensivitylog_model.dart';
import 'package:allergify/models/symptom_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/symptomlog_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class AddSymptomPage extends StatefulWidget {
  AddSymptomPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  _AddSymptomPageState createState() => _AddSymptomPageState(userToken: userToken);
}

class _AddSymptomPageState extends State<AddSymptomPage> {

  _AddSymptomPageState({required this.userToken});

  SymptomlogService get symptomService => GetIt.I<SymptomlogService>();

  TokenModel userToken;

  List<SymptomModel> symptoms = [];

  SymptomModel? pickedSymptom;

  IntensivityLogModel? intensityToReturn;

  double intensity = 0;
  int intensityInt = 0;

  @override
  void initState() {
    Future.delayed(Duration.zero,() async {
      //your async 'await' codes goes here
      var symptomlogResponse =  await symptomService.getAllSymptoms(userToken.accessToken!);
      setState(() {
        symptoms = symptomlogResponse.data!=null?symptomlogResponse.data!:[];
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "ADD SYMPTOM",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Builder(
                builder: (context) {
                  if(pickedSymptom==null){
                    return ListView.builder(
                      itemCount: symptoms.length,
                      itemBuilder: (context, index) {
                        return Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                symptoms[index].name!.toString(),
                                style: GoogleFonts.dosis(
                                  fontSize: 20
                                ),
                              ),
                            ),
                            IconButton(
                              onPressed: (){
                                setState(() {
                                  pickedSymptom=symptoms[index];
                                });
                              },
                              icon: const Icon(
                                Icons.add_rounded,
                                color: ColorPalette.blueAccent,
                              ),
                            )
                          ],
                        );
                      },
                    );
                  }
                  else{
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            "How strong is the symptom?",
                            style: GoogleFonts.dosis(
                              fontSize: 30
                            ),
                          ),
                          Container(
                            height: 20,
                          ),
                          SliderTheme(
                            data: const SliderThemeData(
                              inactiveTickMarkColor: Colors.transparent,
                              activeTickMarkColor: Colors.transparent,
                              thumbColor: ColorPalette.blueAccent,
                              activeTrackColor: ColorPalette.blueAccent,
                              inactiveTrackColor: ColorPalette.blueAccentOp80,
                              overlayColor: ColorPalette.blueAccentOp80
                            ),
                            child: Slider(
                              value: intensity,
                              onChanged:(value) {
                                setState(() {
                                  intensity=value;
                                  intensityInt=intensity.toInt();
                                });
                              },
                              min:0,
                              max:10,
                              divisions: 10,
                            ),
                          ),
                          Container(
                            height: 20,
                          ),
                          Text(
                            intensity.toInt().toString(),
                            style: GoogleFonts.dosis(
                              fontSize: 30
                            ),
                          )
                        ],
                      ),
                    );
                  }
                },
              ),
            ),
          ),
          Container(
            height: 10,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.arrow_back_rounded, (){
                  Navigator.pop(context);
                }),
                Builder(
                  builder: (context) {
                    if(pickedSymptom==null){
                      return Container(
                        height: 70,
                        width: 70,
                      );
                    }
                    else{
                      return CommonElements.gradientIconButton(Icons.check_rounded, (){
                        intensityToReturn = IntensivityLogModel(
                          symptomId: pickedSymptom!.id!,
                          value: intensityInt,
                          symptomName: pickedSymptom!.name!
                        );
                        Navigator.pop(context, intensityToReturn);
                      });
                    }
                  },
                )
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }
}