import 'package:allergify/views/helpers/color_palette.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';

class CommonElements{

  static Size? screenSize;

  static Widget largeLogo(){
    return Container(
      child: Column(
        children: [
          Container(
            height: 120,
            width: 120,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.transparent,
                width: 2,
              ),
              borderRadius: BorderRadius.circular(60),
              gradient: const LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [
                  0.3,
                  1
                ],
                colors: [
                  ColorPalette.lightPink,
                  ColorPalette.lightViolet,
                ],
              )
            ),
            child: const Icon(
              Icons.blender_rounded,
              color: Colors.white,
              size: 80
            ),
          ),
          Container(
            height: 16,
          ),
          Text(
            "ALLERGIFY",
            style: GoogleFonts.benchNine(
              color: ColorPalette.logoTextGray,
              fontWeight: FontWeight.bold,
              fontSize: 52
            ),
          )
        ],
      ),
    );
  }

  static Widget smallLogo({bool allWhiteMode = false}){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 55,
          width: 55,
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.transparent,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(60),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: const [
                0.3,
                1
              ],
              colors: allWhiteMode? [Colors.white, Colors.white]: [ColorPalette.lightPink, ColorPalette.lightViolet],
            )
          ),
          child: Icon(
            Icons.blender_rounded,
            color: allWhiteMode? ColorPalette.lightPink: Colors.white,
            size: 40,
          ),
        ),
        Container(
          width: 15,
        ),
        Text(
          "ALLERGIFY",
          style: GoogleFonts.benchNine(
            fontSize: 50,
            color: allWhiteMode? Colors.white: ColorPalette.logoTextGray,
            fontWeight: FontWeight.w800
          ),
        ),
      ]
    );
  }

  static Widget createTextfieldWithIcon(TextEditingController controller, String hintText, IconData iconData, {bool isPassword = false}){
    return Container(
      width: screenSize!.width*0.8,
      decoration: BoxDecoration(
        color: ColorPalette.inputFieldBackground,
        border: Border.all(
          color: Colors.transparent,
        ),
        borderRadius: BorderRadius.circular(45)
      ),
      child: TextField(
        controller: controller,
        obscureText: isPassword,
        style: GoogleFonts.dosis(
          fontSize: 22
        ),
        decoration: InputDecoration(
          hintText: hintText,
          hintStyle: GoogleFonts.dosis(
            color: ColorPalette.textColor
          ),
          prefixIcon: Icon(
            iconData,
            size: 36,
            color: ColorPalette.textColor,
          ),
          prefixIconConstraints: const BoxConstraints(
            minWidth: 60
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: ColorPalette.inputFieldBackground,
              width: 1.5
            ),
            borderRadius: BorderRadius.circular(45)
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(
              color: ColorPalette.inputFieldBackground,
              width: 1.5
            ),
            borderRadius: BorderRadius.circular(45)
          )
        ),
      ),
    );
  }

  static Widget gradientButton(String text, Function onPressFunction){
    return Container(
      width: screenSize!.width*0.8,
      height: 65,
      child: ElevatedButton(
        onPressed: () => onPressFunction(),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
          padding: const EdgeInsets.all(0.0),
        ),
        child: Ink(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.3,
                1
              ],
              colors: [
                ColorPalette.lightPink,
                ColorPalette.lightViolet,
              ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(80.0)),
          ),
          child: Container(
            constraints: const BoxConstraints(minWidth: 88.0, minHeight: 36.0), // min sizes for Material buttons
            alignment: Alignment.center,
            child: Text(
              text,
              textAlign: TextAlign.center,
              style: GoogleFonts.benchNine(
                fontSize: 30,
                color: Colors.white
              ),
            ),
          ),
        ),
      ),
    );
  }

 static Widget arrowButton(String type, Function onPressFunction){
    return ElevatedButton(
      onPressed: () => onPressFunction(),
      style: ElevatedButton.styleFrom(
        primary: Colors.transparent,
        shape: CircleBorder(),
        padding: const EdgeInsets.all(0.0),
      ),
      child: Ink(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [
              0.2,
              1
            ],
            colors: [
              ColorPalette.lightPink,
              ColorPalette.lightViolet,
            ],
          ),
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
        child: Container(
          height:50,
          width: 50,
          alignment: Alignment.center,
          child: Icon(
            type=="forward"?Icons.arrow_forward_ios_rounded:Icons.arrow_back_ios_rounded,
            color: Colors.white,
            size: 30          )
        ),
      ),
    );
  }


  static Widget gradientIconButton(IconData iconData, Function onPressFunction){
    return ElevatedButton(
      onPressed: () => onPressFunction(),
      style: ElevatedButton.styleFrom(
        primary: Colors.transparent,
        shape: CircleBorder(),
        padding: const EdgeInsets.all(0.0),
      ),
      child: Ink(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [
              0.2,
              1
            ],
            colors: [
              ColorPalette.lightPink,
              ColorPalette.lightViolet,
            ],
          ),
          borderRadius: BorderRadius.all(Radius.circular(40.0)),
        ),
        child: Container(
          height:70,
          width: 70,
          alignment: Alignment.center,
          child: Icon(
            iconData,
            color: Colors.white,
            size: 35          )
        ),
      ),
    );
  }
}