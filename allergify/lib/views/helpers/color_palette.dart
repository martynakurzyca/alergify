import 'package:flutter/material.dart';

class ColorPalette{
  static const Color lightViolet = Color.fromARGB(255, 172, 154, 235);
  static const Color lightPink = Color.fromARGB(255, 242, 176, 177);
  static const Color logoTextGray = Color.fromARGB(255, 190, 190, 190);
  static const Color darkViolet = Color.fromARGB(255, 100, 72, 112);
  static const Color inputFieldBackground = Color.fromARGB(255, 247, 247, 247);
  static const Color blueAccent = Color.fromARGB(255, 98, 150, 255);
  static const Color textColor = Color.fromARGB(255, 210, 210, 210);
  static const Color lightPinkOp80 = Color.fromARGB(80, 242, 176, 177);
  static const Color lightVioletOp80 = Color.fromARGB(80, 172, 154, 235);
  static const Color blueAccentOp80 = Color.fromARGB(80, 98, 150, 255);

}