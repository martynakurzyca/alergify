import 'package:allergify/models/token_model.dart';
import 'package:allergify/models/user_model.dart';
import 'package:allergify/services/user_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class UserSettingsPage extends StatefulWidget {
  UserSettingsPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  _UserSettingsPageState createState() => _UserSettingsPageState(userToken:userToken);
}

class _UserSettingsPageState extends State<UserSettingsPage> {

  _UserSettingsPageState({required this.userToken});

  var passwordController = TextEditingController();
  var passwordConfController = TextEditingController();

  var usernameController = TextEditingController();

  UserService get userService => GetIt.I<UserService>();


  TokenModel userToken;

  UserModel? userModel;

  @override
  void initState(){
    Future.delayed(Duration.zero,() async {
        //your async 'await' codes goes here
        var userResponse =  await userService.getUserData(userToken.accessToken!);
        setState(() {
          userModel = userResponse.data!;
        });
    });
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    height: 40,
                  ),
                  CommonElements.smallLogo(),
                  Container(
                    height: 20,
                  ),
                  Text(
                    "USER SETTINGS",
                    style: GoogleFonts.benchNine(
                      fontSize: 46,
                      fontWeight: FontWeight.bold,
                      color: ColorPalette.logoTextGray
                    ),
                  ),
                  Container(
                    height: 20,
                  ),
                  Container(height: 160,),
                  
                  Container(height: 20,),
                  
                  rectangleMenuButton("DELETE ACCOUNT", Icons.delete_outline_rounded, Colors.white, ColorPalette.lightViolet,(){
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: Text(
                          'Attention',
                          style: GoogleFonts.dosis(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: ColorPalette.blueAccent
                          ),
                        ),
                        content: Text(
                          "You're trying to delete your account and all data connected with it. Are you sure?",
                          style: GoogleFonts.dosis(
                            fontSize: 18,
                          ),
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context),
                            child: const Text('No'),
                          ),
                          TextButton(
                            onPressed: () async {
                              var response = userService.deleteUserData(userToken.accessToken!);
                              
                            },
                            child: const Text('Yes'),
                          ),
                        ],
                      ),
                    );
                    
                  }),
                  Container(
                    height: 20
                  ),
                  rectangleMenuButton("USER MANUAL", Icons.email_outlined, Colors.white, ColorPalette.lightViolet,(){
                    
                  }),
                  Container(
                    height: 30,
                  )
                ],
              ),
      ),
    );
  }

  Widget rectangleMenuButton(String text, IconData icon, Color textColor, Color backgroundColor, Function onPressFunction){
    return Container(
      width: CommonElements.screenSize!.width*0.8,
      height: 80,
      child: ElevatedButton(
        onPressed: () => onPressFunction(),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
          padding: const EdgeInsets.all(0.0),
        ),
        child: Ink(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.3,
                1
              ],
              colors: [
                ColorPalette.lightPink,
                ColorPalette.lightViolet,
              ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(80.0)),
          ),
          child: Container(
            constraints: const BoxConstraints(minWidth: 88.0, minHeight: 36.0), // min sizes for Material buttons
            alignment: Alignment.center,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 30,
                ),
                Icon(
                  icon,
                  size: 50,
                  color: textColor,
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      text,
                      style: GoogleFonts.benchNine(
                        fontSize: 34,
                        fontWeight: FontWeight.bold,
                        color: textColor
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

