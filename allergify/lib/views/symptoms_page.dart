import 'package:allergify/models/symptom_model.dart';
import 'package:allergify/models/symptomlog_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/symptomlog_service.dart';
import 'package:allergify/views/add_symptomlog_page.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'add_food_log_page.dart';
import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class SymptomsPage extends StatefulWidget {
  SymptomsPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  _SymptomsPageState createState() => _SymptomsPageState(userToken: userToken);
}

class _SymptomsPageState extends State<SymptomsPage> {

  _SymptomsPageState({required this.userToken});

  SymptomlogService get symptomLogService => GetIt.I<SymptomlogService>();
  
  TokenModel userToken;
  DateTime? currentDate;
  static var dateFormat = DateFormat("dd-MM-yyyy");
  static var hourFormat = DateFormat("HH:mm");
  List<SymptomLogModel> symptomlog = [];

  List<SymptomModel> symptoms = [];

  var blockForwardArrow = false;
  var blockBackwardArrow = false;

  @override
  void initState() {
    currentDate = DateTime.now();

    Future.delayed(Duration.zero,() async {
      //your async 'await' codes goes here
      var symptomlogResponse =  await symptomLogService.getSymptomlog(currentDate! , userToken.accessToken!);
      var symptomsResponse = await symptomLogService.getAllSymptoms(userToken.accessToken!);
      setState(() {
        symptomlog = symptomlogResponse.data!=null?symptomlogResponse.data!.data:[];
        symptoms = symptomsResponse.data!=null?symptomsResponse.data!:[];
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "SYMPTOM LOG",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CommonElements.arrowButton("back", ()=>arrowButtonClicked("back")),
                Text(
                  dateFormat.format(currentDate!),
                  style: GoogleFonts.benchNine(
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                    color: ColorPalette.logoTextGray
                  ),
                ),
                CommonElements.arrowButton("forward", ()=>arrowButtonClicked("forward")),
              ],
            ),
          ),
          Container(
            height: 10,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: ListView.separated(
                separatorBuilder: (context, index) {
                  return Column(
                    children: [
                      const Divider(
                        color: ColorPalette.logoTextGray,
                        thickness: 1,
                      ),
                      Container(
                        height: 10,
                      )
                    ],
                  );
                },
                shrinkWrap: true,
                itemCount: symptomlog.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          hourFormat.format(symptomlog[index].date!),
                          style: GoogleFonts.dosis(
                            fontSize: 24
                          ),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: symptomlog[index].intensityLogs.length,
                          itemBuilder: (_context, _index) {
                            return ListTile(
                              title: Text(
                                symptoms.where((element) => element.id==symptomlog[index].intensityLogs[_index].symptomId).first.name!,
                                style: GoogleFonts.dosis(
                                  fontSize: 20
                                ),
                              ),
                            );
                          },
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.arrow_back_rounded, (){
                  Navigator.pop(context, false);
                }),
                CommonElements.gradientIconButton(Icons.add_rounded, () async {
                  var result = await Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=>AddSymptomLogPage(userToken: userToken))
                  );
                  result ??= false;
                  if(result){
                    var symptomlogResponse =  await symptomLogService.getSymptomlog(currentDate! , userToken.accessToken!);
                    setState(() {
                      symptomlog = symptomlogResponse.data!=null?symptomlogResponse.data!.data:[];
                    });
                  }
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }

  Future<void> arrowButtonClicked(String type) async {
    var realCurrentDate = DateTime.now();
    if(currentDate!.day==realCurrentDate.day&&currentDate!.month==realCurrentDate.month
      &&currentDate!.year==realCurrentDate.year&&type=="forward"&&blockForwardArrow==false){
      return;
    }
    else if(type=="forward"&&blockForwardArrow==false){
      setState(() {
        blockForwardArrow=true;
        currentDate = currentDate?.add(const Duration(days: 1));
      });
      var symptomlogResponse =  await symptomLogService.getSymptomlog(currentDate!, userToken.accessToken!);
      setState((){
        symptomlog = symptomlogResponse.data!=null?symptomlogResponse.data!.data:[];
        blockForwardArrow=false;
      });
    }
    else if(type=="back"&&blockBackwardArrow==false){
      setState(() {
        blockBackwardArrow=true;
        currentDate = currentDate?.subtract(const Duration(days: 1));
      });
      var symptomlogResponse =  await symptomLogService.getSymptomlog(currentDate!, userToken.accessToken!);
      setState((){
        symptomlog = symptomlogResponse.data!=null?symptomlogResponse.data!.data:[];
        blockBackwardArrow=false;
      });
    }
  }
}