import 'package:allergify/models/add_product_request_model.dart';
import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/ingridient_service.dart';
import 'package:allergify/views/find_ingredient_page.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class AddProductPage extends StatefulWidget {
  AddProductPage({ Key? key, required this.userModel }) : super(key: key);

  TokenModel userModel;

  @override
  _AddProductPageState createState() => _AddProductPageState(userModel: userModel);
}

class _AddProductPageState extends State<AddProductPage> {

  _AddProductPageState({required this.userModel});

  TokenModel userModel;

  List<bool> isCheckedList  = [];

  var nameController = TextEditingController();
    var findTextController = TextEditingController();


  List<IngredientModel> ingredients = [];
  List<IngredientModel> ingredientsToSave = [];

  IngridientService get ingredientService => GetIt.I<IngridientService>();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 10,
          ),
          Text(
            "ADD PRODUCT",
            style: GoogleFonts.benchNine(
              fontSize: 44,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),     
          Container(
            width: CommonElements.screenSize!.width*0.85,
            decoration: BoxDecoration(
              color: ColorPalette.inputFieldBackground,
              border: Border.all(
                color: ColorPalette.inputFieldBackground,
                width: 2
              ),
              borderRadius: BorderRadius.circular(45),
            ),
            child: CommonElements.createTextfieldWithIcon(nameController, "Product Name", Icons.edit_rounded)
          ),
          Container(
            height: 20,
          ),
          Expanded(
            child: Container(
              width: CommonElements.screenSize!.width*0.85,
              child: ListView.builder(
                itemCount: ingredientsToSave.length,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Text(
                          ingredientsToSave[index].name!.toString(),
                          style: GoogleFonts.dosis(
                            fontSize: 20
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: (){
                          setState(() {
                            ingredientsToSave.remove(ingredientsToSave[index]);
                          });
                        },
                        icon: Icon(
                          Icons.close_rounded,
                          color: ColorPalette.blueAccent,
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.close_rounded, (){
                  Navigator.pop(context, false);
                }),
                CommonElements.gradientIconButton(Icons.search_rounded, () async {
                  var results = await Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context)=>FindIngredientPage(userToken: userModel,))
                  );
                  setState(() {
                    if(results!=null){
                      for(IngredientModel product in results){
                        var duplicate = ingredientsToSave.where((element) => element.id==product.id);
                        if(duplicate.isEmpty){
                          ingredientsToSave.add(product);
                        }
                      }
                    }
                  });
                }),
                CommonElements.gradientIconButton(Icons.check_rounded, (){
                  acceptFoodLog();
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      )
    );
  }

Future<void> acceptFoodLog() async {
    if(nameController.text.isEmpty){
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Insert name first",
            style: GoogleFonts.dosis(
              fontSize: 20
            ),
          ),
          backgroundColor: ColorPalette.blueAccent,
        )
      );
    }
    else if(ingredientsToSave.isEmpty){
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Add at least one product",
            style: GoogleFonts.dosis(
              fontSize: 20
            ),
          ),
          backgroundColor: ColorPalette.blueAccent,
        )
      );
    }
    else{
      List<int> productsIds = [];
      for(var product in ingredientsToSave){
        productsIds.add(product.id!);
      }
      var request = AddProductRequestModel(name: nameController.text,ingredientsIds: [], barcode: 7622210063465);
      var response = await ingredientService.addProduct(request, userModel.refreshToken!);
      if(response.isOK!){
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              response.data!,
              style: GoogleFonts.dosis(
                fontSize: 20
              ),
            ),
            backgroundColor: ColorPalette.blueAccent,
          )
        );
        Navigator.pop(context, true);
      }
      else{
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              response.error!,
              style: GoogleFonts.dosis(
                fontSize: 20
              ),
            ),
            backgroundColor: ColorPalette.blueAccent,
          )
        );  
      }
    }
  }
}