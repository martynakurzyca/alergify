import 'package:allergify/models/add_foodlog_request_model.dart';
import 'package:allergify/models/product_details_model.dart';
import 'package:allergify/models/product_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/foodlog_service.dart';
import 'package:allergify/services/product_service.dart';
import 'package:allergify/views/find_product_page.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class AddFoodLogPage extends StatefulWidget {
  AddFoodLogPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  State<AddFoodLogPage> createState() => _AddFoodLogPageState(userToken: userToken);
}

class _AddFoodLogPageState extends State<AddFoodLogPage> {
  final format = DateFormat("yyyy-MM-dd HH:mm");
  
  _AddFoodLogPageState({required this.userToken});

  ProductService get productService => GetIt.I<ProductService>();
  FoodlogService get foodlogService => GetIt.I<FoodlogService>();


  ProductDetialsModel? productDetails;

  DateTime? pickedDateTime;
  TokenModel userToken;

  List<ProductModel> productsToSave = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "ADD FOOD LOG",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),     
          Container(
            width: CommonElements.screenSize!.width*0.85,
            decoration: BoxDecoration(
              color: ColorPalette.inputFieldBackground,
              border: Border.all(
                color: ColorPalette.inputFieldBackground,
                width: 2
              ),
              borderRadius: BorderRadius.circular(45),
            ),
            child: DateTimeField(
              format: format,
              onChanged: (value) => pickedDateTime=value,
              onShowPicker: (context, currentValue) async{
                final date = await showDatePicker(
                context: context,
                firstDate: DateTime(1900),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime.now());
                if(date!=null){
                  final time = await showTimePicker(
                    context: context,
                    initialTime:
                        TimeOfDay.fromDateTime(currentValue ?? DateTime.now().toLocal()),
                    builder: (BuildContext context, Widget? child) {
                      return MediaQuery(
                        data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
                        child: child!,
                      );
                    },
                  );
                  return DateTimeField.combine(date, time);
                }
                else{
                  return currentValue;
                }
              },
              style: GoogleFonts.dosis(
                fontSize: 22
              ),
              decoration: InputDecoration(
                hintText: "Pick date",
                hintStyle: GoogleFonts.dosis(
                  color: ColorPalette.textColor
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                )
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Expanded(
            child: Container(
              width: CommonElements.screenSize!.width*0.85,
              child: ListView.builder(
                itemCount: productsToSave.length,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: InkWell(
                          child: Text(
                            productsToSave[index].name!.toString(),
                            style: GoogleFonts.dosis(
                              fontSize: 20
                            ),
                          ),
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          onDoubleTap:() async {
                            var response = await productService.getProductDetail(productsToSave[index].id!, userToken.accessToken!);
                            setState(() {
                              if(response.data!=null){
                                productDetails=response.data!;
                              }
                            });
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: Text(
                                  'Product details',
                                  style: GoogleFonts.dosis(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: ColorPalette.blueAccent
                                  ),
                                ),
                                content: Container(
                                  height: CommonElements.screenSize!.height*0.3,
                                  width: CommonElements.screenSize!.width*0.5,
                                  child: ListView.builder(
                                    itemCount: productDetails!.ingredients.length,
                                    itemBuilder: (context, _index){
                                      return Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 5),
                                        child: Text(
                                          productDetails!.ingredients[_index].name!,
                                          style: GoogleFonts.dosis(
                                            fontSize: 22
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ),
                              ),
                            );
                          }
                        ),
                      ),
                      IconButton(
                        onPressed: (){
                          setState(() {
                            productsToSave.remove(productsToSave[index]);
                          });
                        },
                        icon: Icon(
                          Icons.close_rounded,
                          color: ColorPalette.blueAccent,
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.close_rounded, (){
                  Navigator.pop(context, false);
                }),
                CommonElements.gradientIconButton(Icons.search_rounded, () async {
                  var results = await Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context)=>FindProductPage(userToken: widget.userToken,))
                  );
                  setState(() {
                    if(results!=null){
                      for(ProductModel product in results){
                        var duplicate = productsToSave.where((element) => element.id==product.id);
                        if(duplicate.isEmpty){
                          productsToSave.add(product);
                        }
                      }
                    }
                  });
                }),
                CommonElements.gradientIconButton(Icons.check_rounded, (){
                  acceptFoodLog();
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }


  Future<void> acceptFoodLog() async {
    if(pickedDateTime==null){
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Insert date first",
            style: GoogleFonts.dosis(
              fontSize: 20
            ),
          ),
          backgroundColor: ColorPalette.blueAccent,
        )
      );
    }
    else if(productsToSave.isEmpty){
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Add at least one product",
            style: GoogleFonts.dosis(
              fontSize: 20
            ),
          ),
          backgroundColor: ColorPalette.blueAccent,
        )
      );
    }
    else{
      List<int> productsIds = [];
      for(var product in productsToSave){
        productsIds.add(product.id!);
      }
      var request = AddFoodlogRequestModel(date: pickedDateTime!,ingredientIds: [], productIds: productsIds);
      var response = await foodlogService.addFoodlog(request, userToken.refreshToken!);
      if(response.isOK!){
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              response.data!,
              style: GoogleFonts.dosis(
                fontSize: 20
              ),
            ),
            backgroundColor: ColorPalette.blueAccent,
          )
        );
        Navigator.pop(context, true);
      }
    }
  }
}

