import 'package:allergify/models/product_details_model.dart';
import 'package:allergify/models/product_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/product_service.dart';
import 'package:allergify/services/user_service.dart';
import 'package:allergify/views/add_product_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class FindProductPage extends StatefulWidget {
  FindProductPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  _FindProductPageState createState() => _FindProductPageState(userToken: userToken);
}

class _FindProductPageState extends State<FindProductPage> {

  _FindProductPageState({required this.userToken});

  ProductService get productService => GetIt.I<ProductService>();

  TokenModel userToken;

  List<ProductModel> foundProducts=[];
  List<ProductModel> productsToAdd=[];

  var findTextController = TextEditingController();

  List<bool> isCheckedList  = [];
  List<int> pickedProducts = [];

  ProductDetialsModel? productDetails;

  bool firstScreen = true;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "FIND PRODUCTS",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),     
          Container(
            width: CommonElements.screenSize!.width*0.85,
            decoration: BoxDecoration(
              color: ColorPalette.inputFieldBackground,
              border: Border.all(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.circular(45)
            ),
            child: TextField(
              controller: findTextController,
              style: GoogleFonts.dosis(
                fontSize: 22
              ),
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(
                    Icons.search_rounded
                  ),
                  onPressed: (){
                    searchProducts();
                  },
                  splashColor: Colors.transparent,
                ),
                hintText: "Find products",
                hintStyle: GoogleFonts.dosis(
                  color: ColorPalette.textColor
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                )
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Expanded(
            child: Container(
              width: CommonElements.screenSize!.width*0.85,
              child: Builder(
                builder: (BuildContext context){
                  if(foundProducts.isEmpty&&!firstScreen){
                    return SingleChildScrollView(
                      child: Center(
                        child: Column(
                          children: [
                            Text(
                              "Not found",
                              style: GoogleFonts.dosis(
                                fontSize: 36
                              ),
                            ),
                            Container(
                              height: 10,
                            ),
                            Text(
                              "You can add new product",
                              style: GoogleFonts.dosis(
                                fontSize: 22
                              ),
                            ),
                            Container(
                              height: 20,
                            ),
                            Container(
                              width: CommonElements.screenSize!.width*0.4,
                              height: 65,
                              child: ElevatedButton(
                                onPressed: () async {
                                  var results = await Navigator.push(
                                    context, 
                                    MaterialPageRoute(builder: (context)=>AddProductPage(userModel: userToken))
                                  );
                                },
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
                                  padding: const EdgeInsets.all(0.0),
                                ),
                                child: Ink(
                                  decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                      stops: [
                                        0.3,
                                        1
                                      ],
                                      colors: [
                                        ColorPalette.lightPink,
                                        ColorPalette.lightViolet,
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(Radius.circular(80.0)),
                                  ),
                                  child: Container(
                                    constraints: const BoxConstraints(minWidth: 88.0, minHeight: 36.0), // min sizes for Material buttons
                                    alignment: Alignment.center,
                                    child: Text(
                                      "ADD PRODUCT",
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.benchNine(
                                        fontSize: 30,
                                        color: Colors.white
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }
                  else if(firstScreen&&foundProducts.isEmpty){
                    firstScreen=!firstScreen;
                    return SingleChildScrollView(
                      child: Center(
                        child: Column(
                          children: [
                            Text(
                              "Type in and find products",
                              style: GoogleFonts.dosis(
                                fontSize: 34
                              ),
                            ),
                            Container(
                              height: 10,
                            ),
                            Container(
                              width: CommonElements.screenSize!.width*0.7,
                              child: Text(
                                "Double tap on searched product to get its details",
                                style: GoogleFonts.dosis(
                                  fontSize: 22
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  else{
                    return ListView.builder(
                      itemCount: foundProducts.length,
                      itemBuilder: (context, index) {
                        return Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: InkWell(
                                child: Text(
                                  foundProducts[index].name!.toString(),
                                  style: GoogleFonts.dosis(
                                    fontSize: 20
                                  ),
                                ),
                                highlightColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                onDoubleTap:() async {
                                  var response = await productService.getProductDetail(foundProducts[index].id!, userToken.accessToken!);
                                  setState(() {
                                    if(response.data!=null){
                                      productDetails=response.data!;
                                    }
                                  });
                                  showDialog<String>(
                                    context: context,
                                    builder: (BuildContext context) => AlertDialog(
                                      title: Text(
                                        'Product details',
                                        style: GoogleFonts.dosis(
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                          color: ColorPalette.blueAccent
                                        ),
                                      ),
                                      content: Container(
                                        height: CommonElements.screenSize!.height*0.3,
                                        width: CommonElements.screenSize!.width*0.5,
                                        child: ListView.builder(
                                          itemCount: productDetails!.ingredients.length,
                                          itemBuilder: (context, _index){
                                            return Padding(
                                              padding: const EdgeInsets.symmetric(vertical: 5),
                                              child: Text(
                                                productDetails!.ingredients[_index].name!,
                                                style: GoogleFonts.dosis(
                                                  fontSize: 22
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          onPressed: () => Navigator.pop(context, 'Cancel'),
                                          child: const Text('Cancel'),
                                        ),
                                        TextButton(
                                          onPressed: () => Navigator.pop(context, 'OK'),
                                          child: const Text('OK'),
                                        ),
                                      ],
                                    ),
                                  );
                                }
                              ),
                            ),
                            Checkbox(
                              value: isCheckedList[index], 
                              onChanged: (value){
                                setState(() {
                                  isCheckedList[index] = value!;
                                  if(isCheckedList[index]==true){
                                    pickedProducts.add(foundProducts[index].id!);
                                    productsToAdd.add(foundProducts[index]);
                                  }
                                  else{
                                    pickedProducts.remove(foundProducts[index].id!);
                                    productsToAdd.remove(foundProducts[index]);
                                  }
                                });
                              }
                            )
                          ],
                        );
                      },
                    );
                  }
                },
              )
            ),
          ),
          Container(
            height: 10,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.arrow_back_rounded, (){
                  Navigator.pop(context);
                }),
                CommonElements.gradientIconButton(Icons.check_rounded, (){
                  acceptProducts();
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }

  Future<void> searchProducts() async {
    var response = await productService.findProducts(findTextController.text, userToken.accessToken!);
    FocusManager.instance.primaryFocus?.unfocus();
    setState(() {
      if(response.data!=null) {
        isCheckedList.clear();
        for (var i = 0; i < response.data!.data.length; i++) {
          if(pickedProducts.contains(response.data!.data[i].id!)){
            isCheckedList.add(true);
          }
          else{
            isCheckedList.add(false);
          }
        }
        foundProducts = response.data!.data;
      }
    });
  }


  void acceptProducts() {
    Navigator.pop(context, productsToAdd);
  }
}