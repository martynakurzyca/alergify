import 'package:allergify/models/login_data_model.dart';
import 'package:allergify/services/user_service.dart';
import 'package:allergify/views/helpers/color_palette.dart';
import 'package:allergify/views/helpers/common_elements.dart';
import 'package:allergify/views/sign_up_page.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'home_page.dart';

class SignInPage extends StatelessWidget {

  TextEditingController loginController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  SignInPage({Key? key}) : super(key: key);

  UserService get userService => GetIt.I<UserService>();

  @override
  Widget build(BuildContext context) {
    CommonElements.screenSize=MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          height: CommonElements.screenSize!.height,
          child: Column(
            children: [
              Container(
                height: 20,
              ),
              CommonElements.largeLogo(),
              Container(
                height: 50,
              ),
              CommonElements.createTextfieldWithIcon(loginController, "Login", Icons.person),
              Container(
                height: 20,
              ),
              CommonElements.createTextfieldWithIcon(passwordController, "Password", Icons.lock, isPassword: true),
              Container(
                height: 20,
              ),
              CommonElements.gradientButton("LOGIN", (){
                tryToLogin(context);
              }),
              Container(
                height: 30,
              ),
              TextButton(
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=>SignUpPage())
                  );
                },
                style: TextButton.styleFrom(
                  primary: ColorPalette.logoTextGray
                ),
                child: Text(
                  "Don't have an account? Sign up!",
                  style: GoogleFonts.dosis(
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                  ),
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
          ),
        ),
      )
    );
  }

  Future<void> tryToLogin(BuildContext context) async {
    
    var loginData = LoginDataModel(
      email: loginController.text,
      password: passwordController.text
    );

    final result = await userService.login(loginData);

    if(result.isOK!){
      FocusScope.of(context).unfocus();
      passwordController.clear();
      loginController.clear();
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context)=>HomePage(userToken: result.data!))
      );
    }
    else{
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            result.error!,
            style: GoogleFonts.dosis(
              fontSize: 20
            ),
          ),
          backgroundColor: ColorPalette.blueAccent,
        )
      );
    }
  }
}
