import 'package:allergify/models/token_model.dart';
import 'package:allergify/models/user_model.dart';
import 'package:allergify/services/user_service.dart';
import 'package:allergify/views/analized_allergens_page.dart';
import 'package:allergify/views/food_log_page.dart';
import 'package:allergify/views/helpers/common_elements.dart';
import 'package:allergify/views/symptoms_page.dart';
import 'package:allergify/views/user_settings_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';

class HomePage extends StatefulWidget {
  HomePage
({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  _HomePageState createState() => _HomePageState(userToken: userToken);
}

class _HomePageState extends State<HomePage> {

  _HomePageState(
    {
      required this.userToken
    }
  );

  String? user;
  TokenModel userToken;


  UserService get userService => GetIt.I<UserService>();



  @override
  void initState(){
    Future.delayed(Duration.zero,() async {
        //your async 'await' codes goes here
        var userResponse =  await userService.getUserData(userToken.accessToken!);
        setState(() {
          user = userResponse.data!.username;
        });
    });
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: SizedBox(
            height: CommonElements.screenSize!.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  height: 0,
                ),
                CommonElements.smallLogo(),
                Container(
                  height: 80,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        onPressed: (){
                          Navigator.pop(context);
                        }, 
                        icon: const Icon(
                          Icons.logout_rounded,
                          color: ColorPalette.lightPink,
                          size: 30,
                        ),
                        highlightColor: ColorPalette.lightPinkOp80,
                      ),
                      Text(
                        user==null?"Hello!":("Hello "+user!+"!"),
                        style: GoogleFonts.benchNine(
                          fontSize: 46,
                          fontWeight: FontWeight.bold,
                          color: ColorPalette.logoTextGray
                        ),
                      ),
                      IconButton(
                        onPressed: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context)=>UserSettingsPage(userToken: userToken))
                          );
                        }, 
                        icon: const Icon(
                          Icons.settings_rounded,
                          color: ColorPalette.lightViolet,
                          size: 30,
                        ),
                        highlightColor: ColorPalette.lightVioletOp80,
                      ),
                    ],
                  ),
                ),
                Container(height: 80,),
                rectangleMenuButton("FOOD LOG", Icons.restaurant_menu_rounded, Colors.white, ColorPalette.lightViolet,(){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=>FoodLogPage(userToken: userToken))
                  );
                }),
                Container(
                  height: 20
                ),
                rectangleMenuButton("SYMPTOMS", Icons.self_improvement_rounded, Colors.white, ColorPalette.lightViolet,(){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=>SymptomsPage(userToken: userToken))
                  );
                }),
                Container(
                  height: 20
                ),
                rectangleMenuButton("ANALIZED ALLERGENS", Icons.table_view_rounded, Colors.white, ColorPalette.lightViolet,(){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=>AnalizedAllergensPage(userToken: userToken))
                  );
                }),
                Container(
                  height: 30,
                )
              ],
            ),
          ),
          )
      ),
    );
  }

  Widget rectangleMenuButton(String text, IconData icon, Color textColor, Color backgroundColor, Function onPressFunction){
    return Container(
      width: CommonElements.screenSize!.width*0.8,
      height: 80,
      child: ElevatedButton(
        onPressed: () => onPressFunction(),
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
          padding: const EdgeInsets.all(0.0),
        ),
        child: Ink(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [
                0.3,
                1
              ],
              colors: [
                ColorPalette.lightPink,
                ColorPalette.lightViolet,
              ],
            ),
            borderRadius: BorderRadius.all(Radius.circular(80.0)),
          ),
          child: Container(
            constraints: const BoxConstraints(minWidth: 88.0, minHeight: 36.0), // min sizes for Material buttons
            alignment: Alignment.center,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  width: 30,
                ),
                Icon(
                  icon,
                  size: 50,
                  color: textColor,
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      text,
                      style: GoogleFonts.benchNine(
                        fontSize: 34,
                        fontWeight: FontWeight.bold,
                        color: textColor
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}