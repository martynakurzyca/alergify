import 'package:allergify/models/add_symptomlog_request_model.dart';
import 'package:allergify/models/intensivitylog_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/symptomlog_service.dart';
import 'package:allergify/views/add_symptom_page.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class AddSymptomLogPage extends StatefulWidget {
  AddSymptomLogPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  State<AddSymptomLogPage> createState() => _AddSymptomLogPageState(userToken: userToken);
}

class _AddSymptomLogPageState extends State<AddSymptomLogPage> {
  final format = DateFormat("yyyy-MM-dd HH:mm");
  
  _AddSymptomLogPageState({required this.userToken});

  SymptomlogService get symptomService => GetIt.I<SymptomlogService>();

  DateTime? pickedDateTime;
  TokenModel userToken;

  List<IntensivityLogModel> symptomsToSave = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "ADD SYMPTOM LOG",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),     
          Container(
            width: CommonElements.screenSize!.width*0.85,
            decoration: BoxDecoration(
              color: ColorPalette.inputFieldBackground,
              border: Border.all(
                color: ColorPalette.inputFieldBackground,
                width: 2
              ),
              borderRadius: BorderRadius.circular(45),
            ),
            child: DateTimeField(
              format: format,
              onChanged: (value) => pickedDateTime=value,
              onShowPicker: (context, currentValue) async{
                final date = await showDatePicker(
                context: context,
                firstDate: DateTime(1900),
                initialDate: currentValue ?? DateTime.now(),
                lastDate: DateTime.now());
                if(date!=null){
                  final time = await showTimePicker(
                    context: context,
                    initialTime:
                        TimeOfDay.fromDateTime(currentValue ?? DateTime.now().toLocal()),
                    builder: (BuildContext context, Widget? child) {
                      return MediaQuery(
                        data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
                        child: child!,
                      );
                    },
                  );
                  return DateTimeField.combine(date, time);
                }
                else{
                  return currentValue;
                }
              },
              style: GoogleFonts.dosis(
                fontSize: 22
              ),
              decoration: InputDecoration(
                hintText: "Pick date",
                hintStyle: GoogleFonts.dosis(
                  color: ColorPalette.textColor
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                )
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Expanded(
            child: Container(
              width: CommonElements.screenSize!.width*0.85,
              child: Builder(
                builder: (context) {
                  if(symptomsToSave.isNotEmpty){
                    return ListView.builder(
                      itemCount: symptomsToSave.length,
                      itemBuilder: (context, index) {
                        return Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: InkWell(
                                child: Text(
                                  symptomsToSave[index].symptomName!.toString(),
                                  style: GoogleFonts.dosis(
                                    fontSize: 20
                                  ),
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  symptomsToSave[index].value.toString()+" per 10",
                                  style: GoogleFonts.dosis(
                                    fontSize: 20
                                  ),
                                ),
                                IconButton(
                                  onPressed: (){
                                    setState(() {
                                      symptomsToSave.remove(symptomsToSave[index]);
                                    });
                                  },
                                  icon: const Icon(
                                    Icons.close_rounded,
                                    color: ColorPalette.blueAccent,
                                  ),
                                ),
                              ],
                            )
                          ],
                        );
                      },
                    );
                  }
                  else{
                    return Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Add some symptoms",
                          style: GoogleFonts.dosis(
                            fontSize: 30
                          ),
                        ),
                        Container(
                          height: 20,
                        ),
                        Text(
                          "By clicking button below",
                          style: GoogleFonts.dosis(
                            fontSize: 22
                          ),
                        ),
                      ],
                    );
                  }
                },
              )
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.close_rounded, (){
                  Navigator.pop(context, false);
                }),
                CommonElements.gradientIconButton(Icons.add_rounded, () async {
                  IntensivityLogModel? results = await Navigator.push(
                    context, 
                    MaterialPageRoute(builder: (context)=>AddSymptomPage(userToken: widget.userToken,))
                  );
                  setState(() {
                    if(results!=null){
                      var matchSymptoms = symptomsToSave.where((element) => element.symptomId==results.symptomId!);
                      if(matchSymptoms.isEmpty){
                        symptomsToSave.add(results);
                      }
                      else{
                        symptomsToSave.remove(matchSymptoms.first);
                        symptomsToSave.add(results);
                      }
                    }
                  });
                }),
                CommonElements.gradientIconButton(Icons.check_rounded, (){
                  acceptSymptomLog();
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }


  Future<void> acceptSymptomLog() async {
    if(pickedDateTime==null){
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Insert date first",
            style: GoogleFonts.dosis(
              fontSize: 20
            ),
          ),
          backgroundColor: ColorPalette.blueAccent,
        )
      );
    }
    else if(symptomsToSave.isEmpty){
      ScaffoldMessenger.of(context).hideCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Add at least one product",
            style: GoogleFonts.dosis(
              fontSize: 20
            ),
          ),
          backgroundColor: ColorPalette.blueAccent,
        )
      );
    }
    else{
      var request = AddSymptomlogRequestModel(date: pickedDateTime!, intensityLogs: symptomsToSave);
      var response = await symptomService.addSymptomlog(request, userToken.refreshToken!);
      if(response.isOK!){
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(
              response.data!,
              style: GoogleFonts.dosis(
                fontSize: 20
              ),
            ),
            backgroundColor: ColorPalette.blueAccent,
          )
        );
        Navigator.pop(context, true);
      }
    }
  }
}

