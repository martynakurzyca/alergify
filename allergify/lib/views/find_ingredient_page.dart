import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/product_details_model.dart';
import 'package:allergify/models/product_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/ingridient_service.dart';
import 'package:allergify/services/product_service.dart';
import 'package:allergify/services/user_service.dart';
import 'package:allergify/views/add_product_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class FindIngredientPage extends StatefulWidget {
  FindIngredientPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  _FindIngredientPageState createState() => _FindIngredientPageState(userToken: userToken);
}

class _FindIngredientPageState extends State<FindIngredientPage> {

  _FindIngredientPageState({required this.userToken});

  IngridientService get ingredientService => GetIt.I<IngridientService>();

  TokenModel userToken;

  List<IngredientModel> foundingredients=[];
  List<IngredientModel> ingredientsToAdd=[];

  var findTextController = TextEditingController();

  List<bool> isCheckedList  = [];
  List<int> pickedingredients = [];

  bool firstScreen = true;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "FIND INGREDIENTS",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),     
          Container(
            width: CommonElements.screenSize!.width*0.85,
            decoration: BoxDecoration(
              color: ColorPalette.inputFieldBackground,
              border: Border.all(
                color: Colors.transparent,
              ),
              borderRadius: BorderRadius.circular(45)
            ),
            child: TextField(
              controller: findTextController,
              style: GoogleFonts.dosis(
                fontSize: 22
              ),
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(
                    Icons.search_rounded
                  ),
                  onPressed: (){
                    searchProducts();
                  },
                  splashColor: Colors.transparent,
                ),
                hintText: "Find ingredients",
                hintStyle: GoogleFonts.dosis(
                  color: ColorPalette.textColor
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: ColorPalette.inputFieldBackground,
                    width: 1.5
                  ),
                  borderRadius: BorderRadius.circular(45)
                )
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Expanded(
            child: Container(
              width: CommonElements.screenSize!.width*0.85,
              child: Builder(
                builder: (BuildContext context){
                  if(foundingredients.isEmpty&&!firstScreen){
                    return SingleChildScrollView(
                      child: Center(
                        child: Column(
                          children: [
                            Text(
                              "Not found",
                              style: GoogleFonts.dosis(
                                fontSize: 36
                              ),
                            ),
                            Container(
                              height: 10,
                            ),
                            Text(
                              "Try some other search values",
                              style: GoogleFonts.dosis(
                                fontSize: 22
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  else if(firstScreen&&foundingredients.isEmpty){
                    firstScreen=!firstScreen;
                    return SingleChildScrollView(
                      child: Center(
                        child: Column(
                          children: [
                            Text(
                              "Type in and find ingredients",
                              style: GoogleFonts.dosis(
                                fontSize: 34
                              ),
                            ),
                            Container(
                              height: 10,
                            ),
                            Container(
                              width: CommonElements.screenSize!.width*0.7,
                              child: Text(
                                "And add them with checkbox",
                                style: GoogleFonts.dosis(
                                  fontSize: 22
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                  else{
                    return ListView.builder(
                      itemCount: foundingredients.length,
                      itemBuilder: (context, index) {
                        return Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                foundingredients[index].name!.toString(),
                                style: GoogleFonts.dosis(
                                  fontSize: 20
                                ),
                              ),
                            ),
                            Checkbox(
                              value: isCheckedList[index], 
                              onChanged: (value){
                                setState(() {
                                  isCheckedList[index] = value!;
                                  if(isCheckedList[index]==true){
                                    pickedingredients.add(foundingredients[index].id!);
                                    ingredientsToAdd.add(foundingredients[index]);
                                  }
                                  else{
                                    pickedingredients.remove(foundingredients[index].id!);
                                    ingredientsToAdd.remove(foundingredients[index]);
                                  }
                                });
                              }
                            )
                          ],
                        );
                      },
                    );
                  }
                },
              )
            ),
          ),
          Container(
            height: 10,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.arrow_back_rounded, (){
                  Navigator.pop(context);
                }),
                CommonElements.gradientIconButton(Icons.check_rounded, (){
                  acceptProducts();
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }

  Future<void> searchProducts() async {
    var response = await ingredientService.findIngredient(findTextController.text, userToken.accessToken!);
    FocusManager.instance.primaryFocus?.unfocus();
    setState(() {
      if(response.data!=null) {
        isCheckedList.clear();
        for (var i = 0; i < response.data!.data.length; i++) {
          if(pickedingredients.contains(response.data!.data[i].id!)){
            isCheckedList.add(true);
          }
          else{
            isCheckedList.add(false);
          }
        }
        foundingredients = response.data!.data;
      }
    });
  }


  void acceptProducts() {
    Navigator.pop(context, ingredientsToAdd);
  }
}