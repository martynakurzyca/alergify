import 'package:allergify/models/foodlog_model.dart';
import 'package:allergify/models/foodlog_request_model.dart';
import 'package:allergify/models/product_details_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/foodlog_service.dart';
import 'package:allergify/services/product_service.dart';
import 'package:allergify/views/add_food_log_page.dart';
import 'package:allergify/views/helpers/color_palette.dart';
import 'package:allergify/views/helpers/common_elements.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class FoodLogPage extends StatefulWidget {
  FoodLogPage({ Key? key, required this.userToken }) : super(key: key);

  TokenModel userToken;

  @override
  _FoodLogPageState createState() => _FoodLogPageState(userToken: userToken);
}

class _FoodLogPageState extends State<FoodLogPage> {

  _FoodLogPageState({ required this.userToken });

  FoodlogService get foodlogService => GetIt.I<FoodlogService>();
  ProductService get productService => GetIt.I<ProductService>();


  TokenModel userToken;
  DateTime? currentDate;
  static var dateFormat = DateFormat("dd-MM-yyyy");
  static var hourFormat = DateFormat("HH:mm");
  List<FoodLogModel> foodlog = [];

  var blockForwardArrow = false;
  var blockBackwardArrow = false;

  ProductDetialsModel? productDetails;

  @override
  void initState() {
    currentDate = DateTime.now();

    Future.delayed(Duration.zero,() async {
      //your async 'await' codes goes here
      var foodlogResponse =  await foodlogService.getFoodlog(currentDate! , userToken.accessToken!);
      setState(() {
        foodlog = foodlogResponse.data!=null?foodlogResponse.data!.data:[];
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "FOOD LOG",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CommonElements.arrowButton("back", ()=>arrowButtonClicked("back")),
                Text(
                  dateFormat.format(currentDate!),
                  style: GoogleFonts.benchNine(
                    fontSize: 32,
                    fontWeight: FontWeight.bold,
                    color: ColorPalette.logoTextGray
                  ),
                ),
                CommonElements.arrowButton("forward", ()=>arrowButtonClicked("forward")),
              ],
            ),
          ),
          Container(
            height: 10,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: ListView.separated(
                separatorBuilder: (context, index) {
                  return Column(
                    children: [
                      const Divider(
                        color: ColorPalette.logoTextGray,
                        thickness: 1,
                      ),
                      Container(
                        height: 10,
                      )
                    ],
                  );
                },
                shrinkWrap: true,
                itemCount: foodlog.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          hourFormat.format(foodlog[index].date!),
                          style: GoogleFonts.dosis(
                            fontSize: 24
                          ),
                        ),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: foodlog[index].products.length,
                          itemBuilder: (_context, _index) {
                            return ListTile(
                              title: InkWell(
                                child: Text(
                                  foodlog[index].products[_index].name!,
                                  style: GoogleFonts.dosis(
                                    fontSize: 20
                                  ),
                                ),
                                highlightColor: Colors.transparent,
                                splashColor: Colors.transparent,
                                onDoubleTap:() async {
                                  var response = await productService.getProductDetail(foodlog[index].products[_index].id!, userToken.accessToken!);
                                  setState(() {
                                    if(response.data!=null){
                                      productDetails=response.data!;
                                    }
                                  });
                                  showDialog<String>(
                                    context: context,
                                    builder: (BuildContext context) => AlertDialog(
                                      title: Text(
                                        'Product details',
                                        style: GoogleFonts.dosis(
                                          fontSize: 24,
                                          fontWeight: FontWeight.bold,
                                          color: ColorPalette.blueAccent
                                        ),
                                      ),
                                      content: Container(
                                        height: CommonElements.screenSize!.height*0.3,
                                        width: CommonElements.screenSize!.width*0.5,
                                        child: ListView.builder(
                                          itemCount: productDetails!.ingredients.length,
                                          itemBuilder: (context, _index){
                                            return Padding(
                                              padding: const EdgeInsets.symmetric(vertical: 5),
                                              child: Text(
                                                productDetails!.ingredients[_index].name!,
                                                style: GoogleFonts.dosis(
                                                  fontSize: 22
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  );
                                }
                              ),
                            );
                          },
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.arrow_back_rounded, (){
                  Navigator.pop(context, false);
                }),
                CommonElements.gradientIconButton(Icons.add_rounded, () async {
                  var result = await Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context)=>AddFoodLogPage(userToken: userToken))
                  );
                  result ??= false;
                  if(result){
                    var foodlogResponse =  await foodlogService.getFoodlog(currentDate! , userToken.accessToken!);
                    setState(() {
                      foodlog = foodlogResponse.data!=null?foodlogResponse.data!.data:[];
                    });
                  }
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }

  Future<void> arrowButtonClicked(String type) async {
    var realCurrentDate = DateTime.now();
    if(currentDate!.day==realCurrentDate.day&&currentDate!.month==realCurrentDate.month
      &&currentDate!.year==realCurrentDate.year&&type=="forward"&&blockForwardArrow==false){
      return;
    }
    else if(type=="forward"&&blockForwardArrow==false){
      setState(() {
        blockForwardArrow=true;
        currentDate = currentDate?.add(const Duration(days: 1));
      });
      var foodlogResponse =  await foodlogService.getFoodlog(currentDate!, userToken.accessToken!);
      setState((){
        foodlog = foodlogResponse.data!=null?foodlogResponse.data!.data:[];
        blockForwardArrow=false;
      });
    }
    else if(type=="back"&&blockBackwardArrow==false){
      setState(() {
        blockBackwardArrow=true;
        currentDate = currentDate?.subtract(const Duration(days: 1));
      });
      var foodlogResponse =  await foodlogService.getFoodlog(currentDate!, userToken.accessToken!);
      setState((){
        foodlog = foodlogResponse.data!=null?foodlogResponse.data!.data:[];
        blockBackwardArrow=false;
      });
    }
  }
}

