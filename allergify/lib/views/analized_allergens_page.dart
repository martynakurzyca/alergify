import 'package:allergify/models/allergen_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/services/allergen_service.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';
import 'helpers/common_elements.dart';

class AnalizedAllergensPage extends StatefulWidget {
  AnalizedAllergensPage({ Key? key, required this.userToken}) : super(key: key);

  TokenModel userToken;

  @override
  _AnalizedAllergensPageState createState() => _AnalizedAllergensPageState(userToken: userToken);
}

class _AnalizedAllergensPageState extends State<AnalizedAllergensPage> {

  _AnalizedAllergensPageState({required this.userToken});

  TokenModel userToken;

  List<AllergenModel> allergens = [];

  AllergenService get allergenService => GetIt.I<AllergenService>();

  @override
  void initState() {
    Future.delayed(Duration.zero,() async {
      //your async 'await' codes goes here
      var allergenResponse =  await allergenService.getAnalizedAllergens(userToken.accessToken!);
      setState(() {
        allergens = allergenResponse.data!=null?allergenResponse.data!.data:[];
        allergens.sort((a, b){
          int cmp = double.parse(b.likelihood!).compareTo(double.parse(a.likelihood!));
          if (cmp != 0) return cmp;
          return b.ingredient!.id!.compareTo(a.ingredient!.id!);
          });
        });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
          ),
          CommonElements.smallLogo(),
          Container(
            height: 20,
          ),
          Text(
            "ANALIZED ALLERGENS",
            style: GoogleFonts.benchNine(
              fontSize: 46,
              fontWeight: FontWeight.bold,
              color: ColorPalette.logoTextGray
            ),
          ),
          Container(
            height: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: ListView.separated(
                separatorBuilder: (context, index) {
                  return Column(
                    children: [
                      const Divider(
                        color: ColorPalette.logoTextGray,
                        thickness: 1,
                      ),
                      Container(
                        height: 10,
                      )
                    ],
                  );
                },
                itemCount: allergens.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              allergens[index].ingredient!.name!,
                              style: GoogleFonts.dosis(
                                fontSize: 20,
                                color:allergens[index].confirmed!?ColorPalette.blueAccent:Colors.black
                              ),
                            ),
                            Container(
                              height: 5,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Text(
                                allergens[index].likelihood==null?"0":(double.parse(allergens[index].likelihood!)*100).toString()+"% probability",
                                style: GoogleFonts.dosis(
                                  fontSize: allergens[index].confirmed!?5:18,
                                  color:allergens[index].confirmed!?Colors.transparent:Colors.black
                                ),
                              ),
                            )
                          ],
                        ),
                        IconButton(
                          onPressed: (){
                            confirmOrCancelAllergen(allergens[index].confirmed!, allergens[index].ingredient!.id!);
                          },
                          icon: Icon(
                            allergens[index].confirmed!?Icons.close_rounded:Icons.check_rounded,
                            color: ColorPalette.blueAccent,
                          ),
                        )
                      ],
                    )
                  );
                },
              )
            ),
          ),
          Container(
            height: 20,
          ),
          Container(
            width: CommonElements.screenSize!.width*0.85,
            child: Row(
              children: [
                CommonElements.gradientIconButton(Icons.arrow_back_rounded, (){
                  Navigator.pop(context, false);
                }),
              ],
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
            ),
          ),
          Container(
            height: 20,
          )
        ],
      ),
    );
  }

  void confirmOrCancelAllergen(bool isConfirmed, int id){
    if(isConfirmed){
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text(
            'Attention',
            style: GoogleFonts.dosis(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: ColorPalette.blueAccent
            ),
          ),
          content: Text(
            "You're trying to delete allergen from comfired allergens. Are you sure?",
            style: GoogleFonts.dosis(
              fontSize: 18,
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('No'),
            ),
            TextButton(
              onPressed: () async {
                var response = await allergenService.unsetAnalizedAllergens(id, userToken.accessToken!);
                if(response.isOK!){
                  var allergenResponse =  await allergenService.getAnalizedAllergens(userToken.accessToken!);
                  setState(() {
                    allergens = allergenResponse.data!=null?allergenResponse.data!.data:[];
                    allergens.sort((a, b){
                      int cmp = double.parse(b.likelihood!).compareTo(double.parse(a.likelihood!));
                      if (cmp != 0) return cmp;
                      return b.ingredient!.id!.compareTo(a.ingredient!.id!);
                      });
                  });
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        response.data!,
                        style: GoogleFonts.dosis(
                          fontSize: 20
                        ),
                      ),
                      backgroundColor: ColorPalette.blueAccent,
                    )
                  );
                }
                else{
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        response.error!,
                        style: GoogleFonts.dosis(
                          fontSize: 20
                        ),
                      ),
                      backgroundColor: ColorPalette.blueAccent,
                    )
                  );
                }
                Navigator.pop(context);
              },
              child: const Text('Yes'),
            ),
          ],
        ),
      );
    }
    else{
      showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text(
            'Attention',
            style: GoogleFonts.dosis(
              fontSize: 24,
              fontWeight: FontWeight.bold,
              color: ColorPalette.blueAccent
            ),
          ),
          content: Text(
            "You're trying to set allergen as comfired allergens. Are you sure?",
            style: GoogleFonts.dosis(
              fontSize: 18,
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: const Text('No'),
            ),
            TextButton(
              onPressed: () async {
                var response = await allergenService.setAnalizedAllergens(id, userToken.accessToken!);
                if(response.isOK!){
                  var allergenResponse =  await allergenService.getAnalizedAllergens(userToken.accessToken!);
                  setState(() {
                    allergens = allergenResponse.data!=null?allergenResponse.data!.data:[];
                    allergens.sort((a, b){
                      int cmp = double.parse(b.likelihood!).compareTo(double.parse(a.likelihood!));
                      if (cmp != 0) return cmp;
                      return b.ingredient!.id!.compareTo(a.ingredient!.id!);
                      });
                  });
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        response.data!,
                        style: GoogleFonts.dosis(
                          fontSize: 20
                        ),
                      ),
                      backgroundColor: ColorPalette.blueAccent,
                    )
                  );
                }
                else{
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        response.error!,
                        style: GoogleFonts.dosis(
                          fontSize: 20
                        ),
                      ),
                      backgroundColor: ColorPalette.blueAccent,
                    )
                  );
                }
                Navigator.pop(context);
              },
              child: const Text('Yes'),
            ),
          ],
        ),
      );
    }
  }
}