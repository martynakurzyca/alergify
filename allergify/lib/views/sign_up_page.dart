import 'package:allergify/models/user_model.dart';
import 'package:allergify/services/user_service.dart';
import 'package:allergify/views/helpers/common_elements.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';

import 'helpers/color_palette.dart';

class SignUpPage extends StatelessWidget {
  SignUpPage({ Key? key }) : super(key: key);

  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordConfirmationController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  UserService get userService => GetIt.I<UserService>();



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: CommonElements.screenSize!.height,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              CommonElements.smallLogo(),
              Container(
                
                height: 50,
              ),
              ShaderMask(
                blendMode: BlendMode.srcIn,
                shaderCallback: (bounds) => const LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [
                    0.3,
                    1
                  ],
                  colors: [
                    ColorPalette.lightPink,
                    ColorPalette.lightViolet,
                  ],
                ).createShader(
                  Rect.fromLTWH(0, 0, bounds.width, bounds.height),
                ),
                child: Text(
                  "REGISTER YOURSELF",
                  style: GoogleFonts.benchNine(
                    fontSize: 46,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              Container(
                height: 30,
              ),
              CommonElements.createTextfieldWithIcon(usernameController, "Login", Icons.person_rounded),
              Container(
                height: 20,
              ),
              CommonElements.createTextfieldWithIcon(emailController, "Email", Icons.mail_rounded),
              Container(
                height: 20,
              ),
              CommonElements.createTextfieldWithIcon(passwordController, "Password", Icons.lock_rounded, isPassword: true),
              Container(
                height: 20,
              ),
              CommonElements.createTextfieldWithIcon(passwordConfirmationController, "Password Confirmation", Icons.lock_rounded, isPassword: true),
              Container(
                height: 30,
              ),
              CommonElements.gradientButton("SIGN IN", () async {
                FocusScope.of(context).unfocus();
                if(passwordController.text==""||passwordConfirmationController.text==""||usernameController.text==""||emailController.text=="")
                {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        "Fill all fields above to sign in",
                        style: GoogleFonts.dosis(
                          fontSize: 20
                        ),
                      ),
                      backgroundColor: ColorPalette.blueAccent,
                    )
                  );
                }
                else if(passwordController.text!=passwordConfirmationController.text)
                {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(
                        "Passwords don't match",
                        style: GoogleFonts.dosis(
                          fontSize: 20
                        ),
                      ),
                      backgroundColor: ColorPalette.blueAccent,
                    )
                  );
                }
                else
                {
                  var user = UserModel(username: usernameController.text, email: emailController.text, password:passwordController.text);
                  final result = await userService.signUpUser(user);
                  if(result.isOK!){
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          "User successfully created, now you can log in",
                          style: GoogleFonts.dosis(
                            fontSize: 20
                          ),
                        ),
                        backgroundColor: ColorPalette.blueAccent,
                      )
                    );
                    Navigator.pop(context);
                  }
                  else{
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(
                          "There was some problem with your registration, try again",
                          style: GoogleFonts.dosis(
                            fontSize: 20
                          ),
                        ),
                        backgroundColor: ColorPalette.blueAccent,
                      )
                    );
                  }
                }
              })
            ],
          ),
        ),
      ),
    );
  }
}