import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/product_model.dart';

class AddProductRequestModel{

  String? name;
  int? barcode;
  List<int> ingredientsIds = [];

  AddProductRequestModel({
    this.name,
    this.barcode,
    required this.ingredientsIds,
  });

  Map<String,dynamic> toJson()=>{
    "name":name,
    "barcode":barcode.toString(),
    "ingredients":List<int>.from(ingredientsIds),
  };
}