class UserModel{

  String? username;
  String? email;
  String? password;


  UserModel({
    this.username,
    this.email,
    this.password,
  });

  Map<String,dynamic> toJson()=>{
    "username":username,
    "email":email,
    "password":password
  };

  UserModel.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        email = json['email'],
        password = json['password'];
}
