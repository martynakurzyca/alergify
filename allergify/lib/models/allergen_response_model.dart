import 'package:allergify/models/allergen_model.dart';
import 'package:allergify/models/foodlog_model.dart';

class AllergenResponseModel{

  int? total;
  List<AllergenModel> data = [];

  
  AllergenResponseModel({
    this.total,
    required this.data
  });

  factory AllergenResponseModel.fromJson(Map<String, dynamic> json)
  {
    var list = json['data'] as List;
    var allergenList = list.map((i) => AllergenModel.fromJson(i)).toList();

    return AllergenResponseModel(
      total: json['total'],
      data: allergenList,
    );
  }
}
