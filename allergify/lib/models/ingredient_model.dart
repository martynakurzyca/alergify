class IngredientModel{

  int? id;
  String? name;

  IngredientModel({
    this.id,
    this.name,
  });

    Map<String,dynamic> toJson()=>{
    "id":id,
    "name":name,
  };

  IngredientModel.fromJson(Map<String, dynamic> json)
    : id = json['id'],
      name = json['name'];
}