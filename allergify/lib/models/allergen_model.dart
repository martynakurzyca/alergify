import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/product_model.dart';

class AllergenModel{

  bool? confirmed;
  String? likelihood;
  IngredientModel? ingredient;

  AllergenModel({
    this.confirmed,
    this.likelihood,
    this.ingredient,
  });

  factory AllergenModel.fromJson(Map<String, dynamic> json)
  {
    return AllergenModel(
      confirmed: json['confirmed'],
      likelihood:  json['likelihood'] ?? "0",
      ingredient: IngredientModel.fromJson(json['ingredient']),
    );
  }

  Map<String,dynamic> toJson()=>{
    "confirmed":confirmed,
    "likelihood":likelihood,
    "ingredients":ingredient!.toJson(),
  };
}