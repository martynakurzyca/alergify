import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/intensivitylog_model.dart';
import 'package:allergify/models/product_model.dart';

class AddSymptomlogRequestModel{

  DateTime? date;
  List<IntensivityLogModel> intensityLogs = [];

  AddSymptomlogRequestModel({
    this.date,
    required this.intensityLogs,
  });

  Map<String,dynamic> toJson()=>{
    "date":date!.toIso8601String(),
    "intensityLogs":List<dynamic>.from(intensityLogs.map((x) => x.toJson())),
  };
}