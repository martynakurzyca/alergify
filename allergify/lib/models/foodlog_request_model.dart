class FoodLogRequest{

  int limit = 100;
  int start = 0;
  String? startDate;
  String? endDate;

  FoodLogRequest(DateTime date){
    var endD = DateTime(date.year, date.month, date.day, 23, 59);
    startDate = date.toIso8601String();
    endDate = endD.toIso8601String();
  }

    Map<String,dynamic> toJson()=>{
    "limit":limit,
    "start":start,
    "startDate":startDate,
    "endDate":endDate,
  };
}