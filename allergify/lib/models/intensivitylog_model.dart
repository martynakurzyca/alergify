class IntensivityLogModel{

  int? id;
  int? symptomId;
  int? value;
  String? symptomName;

  IntensivityLogModel({
    this.id,
    this.symptomId,
    this.value,
    this.symptomName,
  });

    Map<String,dynamic> toJson()=>{
    //"id":id,
    "symptomId":symptomId,
    "value":value,
  };

  IntensivityLogModel.fromJson(Map<String, dynamic> json)
    : id = json['id'],
      symptomId = json['symptomId'],
      value = json['value'];
}