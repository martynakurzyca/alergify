import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/product_model.dart';

class FoodLogModel{

  int? id;
  DateTime? date;
  List<IngredientModel> ingredients = [];
  List<ProductModel> products = [];

  FoodLogModel({
    this.id,
    this.date,
    required this.ingredients,
    required this.products,
  });

  factory FoodLogModel.fromJson(Map<String, dynamic> json)
  {
    var listP = json['products'] as List;
    var productList = listP.map((i) => ProductModel.fromJson(i)).toList();

    var listI = json['ingredients'] as List;
    var ingredientList = listI.map((i) => IngredientModel.fromJson(i)).toList();

    return FoodLogModel(
      id: json['id'],
      date:  DateTime.parse(json['date']),
      products: productList,
      ingredients: ingredientList,
    );
  }

  Map<String,dynamic> toJson()=>{
    "id":id,
    "date":date,
    "ingredients":List<dynamic>.from(ingredients.map((x) => x.toJson())),
    "products":List<dynamic>.from(products.map((x) => x.toJson()))
  };
}