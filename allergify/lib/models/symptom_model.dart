class SymptomModel{

  int? id;
  String? name;

  SymptomModel({
    this.id,
    this.name,
  });

    Map<String,dynamic> toJson()=>{
    "id":id,
    "name":name,
  };

  SymptomModel.fromJson(Map<String, dynamic> json)
    : id = json['id'],
      name = json['name'];
}