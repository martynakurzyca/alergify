
import 'package:allergify/models/ingredient_model.dart';

class ProductDetialsModel{

  int? id;
  String? name;
  List<IngredientModel> ingredients = [];

  ProductDetialsModel({
    this.id,
    this.name, 
    required this.ingredients,
  });

  Map<String,dynamic> toJson()=>{
    "id":id,
    "name":name,
  };

  factory ProductDetialsModel.fromJson(Map<String, dynamic> json)
  {
    var list = json['ingredients'] as List;
    var ingredientList = list.map((i) => IngredientModel.fromJson(i)).toList();

    return ProductDetialsModel(
      id: json['id'],
      name: json['name'],
      ingredients: ingredientList
    );
  }
}