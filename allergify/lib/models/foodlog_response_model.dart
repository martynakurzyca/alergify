import 'package:allergify/models/foodlog_model.dart';

class FoodLogResponse{

  int? total;
  List<FoodLogModel> data = [];

  
  FoodLogResponse({
    this.total,
    required this.data
  });

  factory FoodLogResponse.fromJson(Map<String, dynamic> json)
  {
    var list = json['data'] as List;
    var foodList = list.map((i) => FoodLogModel.fromJson(i)).toList();

    return FoodLogResponse(
      total: json['total'],
      data: foodList,
    );
  }
}
