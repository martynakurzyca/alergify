import 'package:allergify/models/foodlog_model.dart';
import 'package:allergify/models/symptomlog_model.dart';

class SymptomLogResponseModel{

  int? total;
  List<SymptomLogModel> data = [];

  
  SymptomLogResponseModel({
    this.total,
    required this.data
  });

  factory SymptomLogResponseModel.fromJson(Map<String, dynamic> json)
  {
    var list = json['data'] as List;
    var symptomList = list.map((i) => SymptomLogModel.fromJson(i)).toList();

    return SymptomLogResponseModel(
      total: json['total'],
      data: symptomList,
    );
  }
}
