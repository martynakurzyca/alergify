import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/product_model.dart';

class AddFoodlogRequestModel{

  DateTime? date;
  List<int> ingredientIds = [];
  List<int> productIds = [];

  AddFoodlogRequestModel({
    this.date,
    required this.ingredientIds,
    required this.productIds,
  });

  Map<String,dynamic> toJson()=>{
    "date":date!.toIso8601String(),
    "ingredients":List<int>.from(ingredientIds),
    "products":List<int>.from(productIds),
  };
}