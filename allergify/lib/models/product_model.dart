
class ProductModel{

  int? id;
  String? name;

  ProductModel({
    this.id,
    this.name,
  });

  Map<String,dynamic> toJson()=>{
    "id":id,
    "name":name,
  };

  ProductModel.fromJson(Map<String, dynamic> json)
  : id = json['id'],
    name = json['name'];
}