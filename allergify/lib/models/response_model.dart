class Response<T>{

  bool? isOK;
  String? error;
  T? data;


  Response({
    this.isOK,
    this.data,
    this.error
  });

}