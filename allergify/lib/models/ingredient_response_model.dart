import 'package:allergify/models/allergen_model.dart';
import 'package:allergify/models/foodlog_model.dart';
import 'package:allergify/models/ingredient_model.dart';

class IngredientResponseModel{

  int? total;
  List<IngredientModel> data = [];

  
  IngredientResponseModel({
    this.total,
    required this.data
  });

  factory IngredientResponseModel.fromJson(Map<String, dynamic> json)
  {
    var list = json['data'] as List;
    var allergenList = list.map((i) => IngredientModel.fromJson(i)).toList();

    return IngredientResponseModel(
      total: json['total'],
      data: allergenList,
    );
  }
}
