import 'package:allergify/models/ingredient_model.dart';
import 'package:allergify/models/intensivitylog_model.dart';
import 'package:allergify/models/product_model.dart';

class SymptomLogModel{

  int? id;
  DateTime? date;
  List<IntensivityLogModel> intensityLogs = [];

  SymptomLogModel({
    this.id,
    this.date,
    required this.intensityLogs,
  });

  factory SymptomLogModel.fromJson(Map<String, dynamic> json)
  {
    var listI = json['intensityLogs'] as List;
    var intensivityList = listI.map((i) => IntensivityLogModel.fromJson(i)).toList();

    return SymptomLogModel(
      id: json['id'],
      date:  DateTime.parse(json['date']),
      intensityLogs: intensivityList,
    );
  }

  Map<String,dynamic> toJson()=>{
    "id":id,
    "date":date,
    "intensityLogs":List<dynamic>.from(intensityLogs.map((x) => x.toJson())),
  };
}