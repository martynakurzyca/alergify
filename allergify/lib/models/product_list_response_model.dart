import 'package:allergify/models/foodlog_model.dart';
import 'package:allergify/models/product_model.dart';

class ProductListResponseModel{

  int? total;
  List<ProductModel> data = [];

  
  ProductListResponseModel({
    this.total,
    required this.data
  });

  factory ProductListResponseModel.fromJson(Map<String, dynamic> json)
  {
    var list = json['data'] as List;
    var productList = list.map((i) => ProductModel.fromJson(i)).toList();

    return ProductListResponseModel(
      total: json['total'],
      data: productList,
    );
  }
}
