class TokenModel{

  int? expiresIn;
  String? accessToken;
  String? refreshToken;


  TokenModel({
    this.expiresIn,
    this.accessToken,
    this.refreshToken,
  });

  Map<String,dynamic> toJson()=>{
    "expiresIn":expiresIn,
    "accessToken":accessToken,
    "refreshToken":refreshToken
  };

  TokenModel.fromJson(Map<String, dynamic> json)
      : expiresIn = int.parse(json['expiresIn']),
        accessToken = json['accessToken'],
        refreshToken = json['refreshToken'];
}