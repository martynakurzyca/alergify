import 'package:allergify/services/allergen_service.dart';
import 'package:allergify/services/foodlog_service.dart';
import 'package:allergify/services/ingridient_service.dart';
import 'package:allergify/services/product_service.dart';
import 'package:allergify/services/symptomlog_service.dart';
import 'package:allergify/services/user_service.dart';
import 'package:allergify/views/sign_in_page.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';


void setupLocator() {
  GetIt.I.registerLazySingleton(() => UserService());
  GetIt.I.registerLazySingleton(() => FoodlogService());
  GetIt.I.registerLazySingleton(() => ProductService());
  GetIt.I.registerLazySingleton(() => IngridientService());
  GetIt.I.registerLazySingleton(() => SymptomlogService());
  GetIt.I.registerLazySingleton(() => AllergenService());
}

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: SignInPage(),
    );
  }
}