
import 'dart:convert';

import 'package:allergify/models/add_product_request_model.dart';
import 'package:allergify/models/foodlog_request_model.dart';
import 'package:allergify/models/foodlog_response_model.dart';
import 'package:allergify/models/ingredient_response_model.dart';
import 'package:allergify/models/response_model.dart';
import 'package:allergify/views/add_product_page.dart';
import 'package:http/http.dart' as http;



class IngridientService{

  static const api = 'https://alergify.pl/api';
  static const headers = {
    'Content-Type': 'application/json'
  };
  static var headersToken = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer TOKEN'
  };


  Future<Response<IngredientResponseModel>> findIngredient(String ingredientName, String token) async{
    final queryParams = {
      'name': ingredientName,
      "limit":"1000",
      "start":"0"
    };
    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/ingredient',
      queryParameters: queryParams,
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.get(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 200) {
        Map<String, dynamic> foodMap = jsonDecode(data.body);
        var foods = IngredientResponseModel.fromJson(foodMap);
        return Response(isOK: true, data: foods);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }
  
  Future<Response<String>> addProduct(AddProductRequestModel requestModel, String token) async{
    //var userJson = jsonEncode(user.toJson());
    headersToken['Authorization'] = 'Bearer ' + token;
    return http.post(Uri.parse(api + '/product'), headers: headersToken, body: jsonEncode(requestModel.toJson())).then((data) async {
      var dataCode = data.statusCode;
      if (data.statusCode == 201) {
        return Response(isOK: true, data: "Added successfully");
      }
      return Response(isOK:false, error: "Some problems have occured");
    });
  }
}