import 'dart:convert';
import 'package:allergify/models/add_symptomlog_request_model.dart';
import 'package:allergify/models/response_model.dart';
import 'package:allergify/models/symptom_model.dart';
import 'package:allergify/models/symptomlog_response_model.dart';
import 'package:http/http.dart' as http;



class SymptomlogService{

  static const api = 'https://alergify.pl/api';
  static const headers = {
    'Content-Type': 'application/json'
  };
  static var headersToken = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer TOKEN'
  };


  Future<Response<SymptomLogResponseModel>> getSymptomlog(DateTime date, String token) async{
    var startD = DateTime(date.year, date.month, date.day, 0, 0);
    var endD = DateTime(date.year, date.month, date.day, 23, 59);
    var startDate = startD.toIso8601String();
    var endDate = endD.toIso8601String();

    final queryParams = {
      'startDate': startDate,
      'endDate': endDate,
      'limit':"100",
      'start':"0"
    };
    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/symptom-log',
      queryParameters: queryParams,
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.get(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 200) {
        Map<String, dynamic> symptomMap = jsonDecode(data.body);
        var symptoms = SymptomLogResponseModel.fromJson(symptomMap);
        return Response(isOK: true, data: symptoms);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }

  Future<Response<String>> addSymptomlog(AddSymptomlogRequestModel requestModel, String token) async{
    //var userJson = jsonEncode(user.toJson());
    headersToken['Authorization'] = 'Bearer ' + token;
    var json = jsonEncode(requestModel.toJson());
    return http.post(Uri.parse(api + '/symptom-log'), headers: headersToken, body: jsonEncode(requestModel.toJson())).then((data) async {
      var json = jsonEncode(requestModel.toJson());
      if (data.statusCode == 201) {
        return Response(isOK: true, data: "Added successfully");
      }
      return Response(isOK:false, error: "Some problems have occured");
    });
  }

Future<Response<List<SymptomModel>>> getAllSymptoms(String token) async{
    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/symptoms',
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.get(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 200) {
        Iterable symptomMap = jsonDecode(data.body);
        List<SymptomModel> symptoms = List<SymptomModel>.from(symptomMap.map((model)=> SymptomModel.fromJson(model)));
        return Response(isOK: true, data: symptoms);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }
}