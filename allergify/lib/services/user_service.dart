
import 'dart:convert';

import 'package:allergify/models/login_data_model.dart';
import 'package:allergify/models/response_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:allergify/models/user_model.dart';
import 'package:http/http.dart' as http;



class UserService{

  static const api = 'https://alergify.pl/api';
  static const headers = {
    'Content-Type': 'application/json'
  };
  static var headersToken = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer TOKEN'
  };


  Future<Response<String>> signUpUser(UserModel user) async{
    var userJson = jsonEncode(user.toJson());
    return http.post(Uri.parse(api + '/signup'), headers: headers, body: jsonEncode(user.toJson())).then((data){
      if (data.statusCode == 200) {
        return Response(isOK: true, data: "User successfully registered");
      }
      return Response(isOK:false, data: "The problem has occured, try again");
    });
  }

  Future<Response<TokenModel>> login(LoginDataModel login) async{
    //var userJson = jsonEncode(user.toJson());
    return http.post(Uri.parse(api + '/login'), headers: headers, body: jsonEncode(login.toJson())).then((data) async {
      if (data.statusCode == 200) {
        Map<String, dynamic> tokenMap = jsonDecode(data.body);
        var token = TokenModel.fromJson(tokenMap);
        return Response(isOK: true, data: token);
      }
      return Response(isOK:false, error: "Password or email is incorrect");
    });
  }


  Future<Response<UserModel>> getUserData(String token) async{
    headersToken['Authorization'] = 'Bearer ' + token;
    return http.get(Uri.parse(api + '/me'), headers: headersToken).then((data){
      if (data.statusCode == 200) {
        Map<String, dynamic> userMap = jsonDecode(data.body);
        var gottenUser = UserModel.fromJson(userMap);
        return Response(isOK: true, data: gottenUser);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }


Future<Response<String>> editUserData(UserModel user, String token) async{
    headersToken['Authorization'] = 'Bearer ' + token;
    return http.put(Uri.parse(api + '/me'), headers: headersToken, body: jsonEncode(user.toJson())).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 204) {
        return Response(isOK: true, data: "User successfully updated");
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }

  Future<Response<String>> deleteUserData(String token) async{
    headersToken['Authorization'] = 'Bearer ' + token;
    return http.delete(Uri.parse(api + '/me'), headers: headersToken).then((data){
            var dataCode = data.statusCode;
      if (data.statusCode == 204) {
        return Response(isOK: true, data: "User deleted updated");
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }

}

