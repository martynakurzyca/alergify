import 'dart:convert';
import 'package:allergify/models/foodlog_request_model.dart';
import 'package:allergify/models/foodlog_response_model.dart';
import 'package:allergify/models/product_details_model.dart';
import 'package:allergify/models/product_list_response_model.dart';
import 'package:allergify/models/response_model.dart';
import 'package:allergify/models/token_model.dart';
import 'package:http/http.dart' as http;



class ProductService{

  static const api = 'https://alergify.pl/api';
  static const headers = {
    'Content-Type': 'application/json'
  };
  static var headersToken = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer TOKEN'
  };


  Future<Response<ProductListResponseModel>> findProducts(String productName, String token) async{
    final queryParams = {
      'name': productName,
      'limit':"1000",
      "start":"0"
    };
    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/product',
      queryParameters: queryParams,
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.get(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 200) {
        Map<String, dynamic> foodMap = jsonDecode(data.body);
        var foods = ProductListResponseModel.fromJson(foodMap);
        return Response(isOK: true, data: foods);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }


    Future<Response<ProductDetialsModel>> getProductDetail(int id, String token) async{
    headersToken['Authorization'] = 'Bearer ' + token;
    var uri = Uri.parse(api + '/product/'+id.toString());
    return await http.get(Uri.parse(api + '/product/'+id.toString()), headers: headersToken).then((data){
      if (data.statusCode == 200) {
        Map<String, dynamic> productMap = jsonDecode(data.body);
        var productDetail = ProductDetialsModel.fromJson(productMap);
        return Response(isOK: true, data: productDetail);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }
}