
import 'dart:convert';

import 'package:allergify/models/add_foodlog_request_model.dart';
import 'package:allergify/models/foodlog_request_model.dart';
import 'package:allergify/models/foodlog_response_model.dart';
import 'package:allergify/models/response_model.dart';
import 'package:http/http.dart' as http;



class FoodlogService{

  static const api = 'https://alergify.pl/api';
  static const headers = {
    'Content-Type': 'application/json'
  };
  static var headersToken = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer TOKEN'
  };


  Future<Response<FoodLogResponse>> getFoodlog(DateTime date, String token) async{
    var startD = DateTime(date.year, date.month, date.day, 0, 0);
    var endD = DateTime(date.year, date.month, date.day, 23, 59);
    var startDate = startD.toIso8601String();
    var endDate = endD.toIso8601String();

    final queryParams = {
      'startDate': startDate,
      'endDate': endDate,
      'limit':"100",
      'start':"0"
    };
    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/food-log',
      queryParameters: queryParams,
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.get(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 200) {
        Map<String, dynamic> foodMap = jsonDecode(data.body);
        var foods = FoodLogResponse.fromJson(foodMap);
        return Response(isOK: true, data: foods);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }

    Future<Response<String>> addFoodlog(AddFoodlogRequestModel requestModel, String token) async{
    //var userJson = jsonEncode(user.toJson());
    headersToken['Authorization'] = 'Bearer ' + token;
    return http.post(Uri.parse(api + '/food-log'), headers: headersToken, body: jsonEncode(requestModel.toJson())).then((data) async {
      if (data.statusCode == 201) {
        return Response(isOK: true, data: "Added successfully");
      }
      return Response(isOK:false, error: "Some problems have occured");
    });
  }
}