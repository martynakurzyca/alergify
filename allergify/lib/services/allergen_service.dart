
import 'dart:convert';

import 'package:allergify/models/add_foodlog_request_model.dart';
import 'package:allergify/models/allergen_response_model.dart';
import 'package:allergify/models/response_model.dart';
import 'package:http/http.dart' as http;



class AllergenService{

  static const api = 'https://alergify.pl/api';
  static const headers = {
    'Content-Type': 'application/json'
  };
  static var headersToken = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer TOKEN'
  };


  Future<Response<AllergenResponseModel>> getAnalizedAllergens(String token) async{
    final queryParams = {
      'limit':"1000",
      'start':"0"
    };

    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/allergens',
      queryParameters: queryParams,
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.get(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 200) {
        Map<String, dynamic> allergenMap = jsonDecode(data.body);
        var allergen = AllergenResponseModel.fromJson(allergenMap);
        return Response(isOK: true, data: allergen);
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }

  Future<Response<String>> setAnalizedAllergens(int id, String token) async{
    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/allergens/'+id.toString(),
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.post(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 204) {
        return Response(isOK: true, data: "Allergen successfully set");
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }

  Future<Response<String>> unsetAnalizedAllergens(int id, String token) async{
    var uri = Uri(
      scheme: 'https',
      host: 'alergify.pl',
      path: '/api/allergens/'+id.toString(),
    );

    headersToken['Authorization'] = 'Bearer ' + token;
    return await http.delete(uri, headers: headersToken).then((data){
      var dataCode = data.statusCode;
      if (data.statusCode == 200) {
        return Response(isOK: true, data: "Allergen successfully unset");
      }
      return Response(isOK:false, error: "The problem has occured, try again");
    });
  }
}